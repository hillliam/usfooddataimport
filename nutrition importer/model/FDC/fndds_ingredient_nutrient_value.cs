using System;

namespace nutrition_importer.model.FDC
{
    public class fndds_ingredient_nutrient_value
    {
        public string ingredient_code { get; set; }
        public string Ingredient_description { get; set; }
        public string Nutrient_code { get; set; }
        public string Nutrient_value { get; set; }
        public string Nutrient_value_source { get; set; }
        public int FDC_ID { get; set; }
        public string Derivation_code { get; set; }
        public string SR_AddMod_year { get; set; }
        public string Foundation_year_acquired { get; set; }
        public DateTime Start_date { get; set; }
        public DateTime End_date { get; set; }
    }
}