using System;

namespace nutrition_importer.model.FDC
{
    public class food_update_log_entry
    {
        public int id { get; set; }
        public string description { get; set; }
        public DateTime last_updated { get; set; }
    }
}