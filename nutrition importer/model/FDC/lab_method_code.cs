namespace nutrition_importer.model.FDC
{
    public class lab_method_code
    {
        public int id { get; set; }
        public int lab_method_id { get; set; }
        public string code { get; set; }
    }
}