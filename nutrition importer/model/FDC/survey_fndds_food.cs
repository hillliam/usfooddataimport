namespace nutrition_importer.model.FDC
{
    public class survey_fndds_food
    {
        public int fdc_id { get; set; }
        public string food_code { get; set; }
        public string wweia_category_code { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
    }
}