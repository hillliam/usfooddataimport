namespace nutrition_importer.model.FDC
{
    public class nutrient
    {
        public int id { get; set; }
        public string name { get; set; }
        public string unit_name { get; set; }
        public int nutrient_nbr { get; set; }
        public string rank { get; set; }
    }
}