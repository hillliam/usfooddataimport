namespace nutrition_importer.model.FDC
{
    public class retention_factor
    {
        public int id { get; set; }
        public string code { get; set; }
        public int food_group_id { get; set; }
        public string description { get; set; }
    }
}