namespace nutrition_importer.model.FDC
{
    public class food_attribute
    {
        public int id { get; set; }
        public int fdc_id { get; set; }
        public int seq_num { get; set; }
        public int food_attribute_type_id { get; set; }
        public string name { get; set; }
        public string value { get; set; }
    }
}