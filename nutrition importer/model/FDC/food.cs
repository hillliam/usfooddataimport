using System;

namespace nutrition_importer.model.FDC
{
    public class food
    {
        public int fdc_id { get; set; }
        public string data_type { get; set; }
        public string description { get; set; }
        public string food_category_id { get; set; }
        public DateTime publication_date { get; set; }
    }
}