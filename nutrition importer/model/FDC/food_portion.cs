namespace nutrition_importer.model.FDC
{
    public class food_portion
    {
        public int id { get; set; }
        public int fdc_id { get; set; }
        public int seq_num { get; set; }
        public string amount { get; set; }
        public int measure_unit_id { get; set; }
        public string portion_description { get; set; }
        public string modifier { get; set; }
        public string gram_weight { get; set; }
        public string data_points { get; set; }
        public string footnote { get; set; }
        public string min_year_acquired { get; set; }
    }
}