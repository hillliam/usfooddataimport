namespace nutrition_importer.model.FDC
{
    public class food_attribute_type
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}