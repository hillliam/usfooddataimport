using System;

namespace nutrition_importer.model.FDC
{
    public class agricultural_acquisition
    {
        public int fdc_id { get; set; }
        public DateTime acquisition_date { get; set; }
        public string market_class { get; set; }
        public string treatment { get; set; }
        public string state { get; set; }
    }
}