namespace nutrition_importer.model.FDC
{
    public class market_acquisition
    {
        public int fdc_id { get; set; }
        public string brand_description { get; set; }
        public string expiration_date { get; set; }
        public string label_weight { get; set; }
        public string location { get; set; }
        public string acquisition_date { get; set; }
        public string sales_type { get; set; }
        public string sample_lot_nbr { get; set; }
        public string sell_by_date { get; set; }
        public string store_city { get; set; }
        public string store_name { get; set; }
        public string store_state { get; set; }
        public string upc_code { get; set; }
    }
}