namespace nutrition_importer.model.FDC
{
    public class food_category
    {
        public int id { get; set; }
        public string code { get; set; }
        public string description { get; set; }
    }
}