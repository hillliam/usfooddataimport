namespace nutrition_importer.model.FDC
{
    public class food_nutrient_derivation
    {
        public int id { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public int source_id { get; set; }
    }
}