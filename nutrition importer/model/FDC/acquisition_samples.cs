using System;

namespace nutrition_importer.model.FDC
{
    public class acquisition_samples
    {
        public int fdc_id_of_sample_food { get; set; }
        public int fdc_id_of_acquisition_food { get; set; }
    }
}