namespace nutrition_importer.model.FDC
{
    public class food_protein_conversion_factor
    {
        public int food_nutrient_conversion_factor_id { get; set; }
        public string value { get; set; }
    }
}