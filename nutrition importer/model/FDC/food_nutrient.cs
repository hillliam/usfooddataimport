namespace nutrition_importer.model.FDC
{
    public class food_nutrient
    {
        public int id { get; set; }
        public int fdc_id { get; set; }
        public int nutrient_id { get; set; }
        public string amount { get; set; }
        public string data_points { get; set; }
        public int derivation_id { get; set; }
        public string min { get; set; }
        public string max { get; set; }
        public string median { get; set; }
        public string footnote { get; set; }
        public string min_year_acquired { get; set; }
    }
}