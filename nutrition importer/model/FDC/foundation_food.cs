namespace nutrition_importer.model.FDC
{
    public class foundation_food
    {
        public int fdc_id { get; set; }
        public int NDB_number { get; set; }
        public string footnote { get; set; }
    }
}