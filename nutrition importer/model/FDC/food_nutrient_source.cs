namespace nutrition_importer.model.FDC
{
    public class food_nutrient_source
    {
        public int id { get; set; }
        public string code { get; set; }
        public string description { get; set; }
    }
}