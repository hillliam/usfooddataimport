namespace nutrition_importer.model.FDC
{
    public class fndds_derivation
    {
        public string derivation_code { get; set; }
        public string derivation_description { get; set; }
    }
}