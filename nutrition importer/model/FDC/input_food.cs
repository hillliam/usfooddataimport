namespace nutrition_importer.model.FDC
{
    public class input_food
    {
        public int id { get; set; }
        public int fdc_id { get; set; }
        public int fdc_id_of_input_food { get; set; }
        public int seq_num { get; set; }
        public string amount { get; set; }
        public string sr_code { get; set; }
        public string sr_description { get; set; }
        public string unit { get; set; }
        public string portion_code { get; set; }
        public string portion_description { get; set; }
        public string gram_weight { get; set; }
        public string retention_code { get; set; }
        public string survey_flag { get; set; }
    }
}