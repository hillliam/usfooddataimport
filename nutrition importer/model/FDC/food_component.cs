namespace nutrition_importer.model.FDC
{
    public class food_component
    {
        public int id { get; set; }
        public int fdc_id { get; set; }
        public string name { get; set; }
        public string pct_weight { get; set; }
        public string is_refuse { get; set; }
        public string gram_weight { get; set; }
        public string data_points { get; set; }
        public string min_year_acquired { get; set; }
    }
}