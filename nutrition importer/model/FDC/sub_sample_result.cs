namespace nutrition_importer.model.FDC
{
    public class sub_sample_result
    {
        public int food_nutrient_id { get; set; }
        public string adjusted_amount { get; set; }
        public int lab_method_id { get; set; }
        public string nutrient_name { get; set; }
    }
}