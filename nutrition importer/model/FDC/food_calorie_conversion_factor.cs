namespace nutrition_importer.model.FDC
{
    public class food_calorie_conversion_factor
    {
        public int food_nutrient_conversion_factor_id { get; set; }
        public string protein_value { get; set; }
        public string fat_value { get; set; }
        public string carbohydrate_value { get; set; }
    }
}