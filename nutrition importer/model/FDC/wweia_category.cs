namespace nutrition_importer.model.FDC
{
    public class wweia_category
    {
        public string wweia_food_category { get; set; }
        public string wweia_food_category_description { get; set; }
    }
}