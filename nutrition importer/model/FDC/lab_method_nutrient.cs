namespace nutrition_importer.model.FDC
{
    public class lab_method_nutrient
    {
        public int id { get; set; }
        public int lab_method_id { get; set; }
        public int nutrient_id { get; set; }
    }
}