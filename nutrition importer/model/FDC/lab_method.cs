namespace nutrition_importer.model.FDC
{
    public class lab_method
    {
        public int id { get; set; }
        public string description { get; set; }
        public string technique { get; set; }
    }
}