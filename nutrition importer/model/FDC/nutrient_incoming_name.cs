namespace nutrition_importer.model.FDC
{
    public class nutrient_incoming_name
    {
        public int id { get; set; }
        public string name { get; set; }
        public int nutrient_id { get; set; }
    }
}