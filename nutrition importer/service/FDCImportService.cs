using System;
using System.Collections.Generic;
using System.Linq;
using ChoETL;
using nutrition_importer.model.FDC;

namespace nutrition_importer.service
{
    public class FDCImportService
    {
        public void loadAllData()
        {
            //var food = this.GetData<food>().ToArray();
            var asample = this.GetData<acquisition_samples>().ToArray();
            var agraculture = this.GetData<agricultural_acquisition>().ToArray();
            var brandedFood = this.GetData<branded_food>().ToArray();
            var fndsDetivation = this.GetData<fndds_derivation>().ToArray();
            var fndsIngrienent = this.GetData<fndds_ingredient_nutrient_value>().ToArray();
            var foodAttribute = this.GetData<food_attribute>().ToArray();
            var foodAttributeType = this.GetData<food_attribute_type>().ToArray();
            var foodCalorieConversion = this.GetData<food_calorie_conversion_factor>().ToArray();
            var foodCategory = this.GetData<food_category>().ToArray();
            var foodComponent = this.GetData<food_component>().ToArray();
            var foodNutrient = this.GetData<food_nutrient>().ToArray();
            var foodNutrientConversion = this.GetData<food_nutrient_conversion_factor>().ToArray();
            var foodNutrientDerivation = this.GetData<food_nutrient_derivation>().ToArray();
            var foodNutrientSource = this.GetData<food_nutrient_source>().ToArray();
            var foodPortion = this.GetData<food_portion>().ToArray();
            var foodProteinConversion = this.GetData<food_protein_conversion_factor>().ToArray();
            var foodUpdateLog = this.GetData<food_update_log_entry>().ToArray();
            var foundationFood = this.GetData<foundation_food>().ToArray();
            var inputFood = this.GetData<input_food>().ToArray();
            var labMethod = this.GetData<lab_method>().ToArray();
            var labMethodCode = this.GetData<lab_method_code>().ToArray();
            var labNutrirnt = this.GetData<lab_method_nutrient>().ToArray();
            var marketAcuistion = this.GetData<market_acquisition>().ToArray();
            var measureUnit = this.GetData<measure_unit>().ToArray();
            var nutrient = this.GetData<nutrient>().ToArray();
            var nutrientName = this.GetData<nutrient_incoming_name>().ToArray();
            var retentionFactor = this.GetData<retention_factor>().ToArray();
            var sampleFood = this.GetData<sample_food>().ToArray();
            var legcyFood = this.GetData<sr_legacy_food>().ToArray();
            var subSampleF = this.GetData<sub_sample_food>().ToArray();
            var subSampleR = this.GetData<sub_sample_result>().ToArray();
            var surveyFood = this.GetData<survey_fndds_food>().ToArray();
            var wweia = this.GetData<wweia_category>().ToArray();
        }

        private IEnumerable<T> GetData<T>() where T : class
        {
            var filename = typeof(T).Name + ".csv";
            using (var reader = new Streamreader(filename) )
            using (var csvfile = new  CsvReader(reader, CultureInfo.InvariantCulture))
            {  
                var records = csvfile.GetRecords<T>();
                return records;
            }
        }
    }
}